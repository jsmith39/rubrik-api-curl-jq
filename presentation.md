# Introduction

A few days ago I pasted an example of using [cURL](https://curl.se/) and [jq](https://stedolan.github.io/jq/) to interact with the restful API on our rubrik to obtain a list of virtual machines and the data protection SLAs associated with them.  Members of the team asked questions about how it all worked.  This is my attempt to break it all down and hopefully shed some light on how it works.

---

# Caveats

The examples within this presentation are all specific to a [bash shell](https://www.gnu.org/software/bash/).  Both curl and jq are available for windows, however the semantics of the commands may be slightly different.  For example the syntax for accessing environment variables or quoting strings will likely require modifications if running these commands on windows.


---

# The command line

```bash
curl -u "${RUBRIK_USERNAME}:${RUBRIK_PASSWORD}" -X GET https://adp-rubrikwvnt.wvu-ad.wvu.edu/api/v1/vmware/vm | jq -r '.data[] | select( .effectiveSlaDomainName != "Unprotected") | [.name, .effectiveSlaDomainName] | @csv'

```

---

# What it does

1. Connect to the specified URL and retrieve data.
2. Parse the retrieved data using jq and return only the information we are interested in.
3. Format it as a list of comma seperated values (CSV).

---

# cURL

- cURL is a command line tool used for transferring data from URLs

## Breakdown from our example

```bash
curl -u "${RUBRIK_USERNAME}:${RUBRIK_PASSWORD}" -X GET https://adp-rubrikwvnt.wvu-ad.wvu.edu/api/v1/vmware/vm
```

### `-u "${RUBRIK_USERNAME}:${RUBRIK_PASSWORD}"`

- The `-u` option allows specifying the user name and password to use for server authentication.  Since rubrik does not return information to unauthenticated users this is required.

- `"${RUBRIK_USERNAME}:${RUBRIK_PASSWORD}"` instruct bash to substitute the contents of the environment variables `RUBRIK_USERNAME` and `RUBRIK_PASSWORD`  when executing this command.

---


# Breakdown from our example (cont'd)

## `-X GET`

- The `-X` option specifies the request method to use when communicating with an HTTP(s) server. Common additional HTTP requests include PUT, POST, and DELETE,

- A list of HTTP request methods can be found here: https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods

## `https://adp-rubrikwvnt.wvu-ad.wvu.edu/api/v1/vmware/vm`

- This is the URL cURL will connect to.

---

# How to determine the URL to use.

## It's in the documentation

- https://build.rubrik.com/

## API Playground

- Each rubrik cluster provides an API playground.
- For example: https://adp-rubrikowp.wvu-ad.wvu.edu/docs/v1/playground/

---

# How to determine the URL to use. (cont'd)

- I knew I wanted to get information about vmware virtutal machines.  So in the playground I started looking for URLs that looked related.
- There is one listed as `/vmware/vm`. That looks promising. In the playground you can click to get additional details.
- `GET /vmware/vm - get a list of VMs`.  That looks like exactly what we need.  Again in the playground you can click to get additional details. Including an example response and the response content type.

---

# Response Content Type: application/json

- Indicates we should expect JSON in the response body to our request.
- JSON stands for Javascript Object Notation.  It is a lightweight storing and transporting data.
- JSON is frequently used when data is being sent or received to/from an HTTP server.
- Not always the most human readable format but it's usually not too bad.

---

# `|`

- The vertical bar or pipe character allows for redirecting the output of one command to the input of another.
- This simple concept enables the creation of very powerful utilities by enabling us to chain simple tools together to form complex pipelines.

---

# jq

- jq is a lightweight and flexible command-line JSON processor.
- jq is like sed for JSON data - you can use it to slice and filter and map and transform structured data with the same ease that sed, awk, and grep.

---

# Breakdown from our example

```bash
jq -r '.data[] | select( .effectiveSlaDomainName != "Unprotected") | [.name,     .effectiveSlaDomainName] | @csv'
```

## `jq -r`

- Invoke the `jq` command with the `-r` option.
- `-r` With this option, if the filter´s result is a string then it will be written directly to standard output rather than being formatted as a JSON string with quotes.
- This is kind of a wordy way to say it makes the output a little more human readable.

---

# Breakdown from our example (cont'd)

## `'.data[] | select( .effectiveSlaDomainName != "Unprotected") | [.name,     .effectiveSlaDomainName] | @csv'`

- This string is the filter we are passing to jq.
- The single quotes `'` surrounding indicate that we do not want the shell to interpret anything within them and the string they surround should be passed directly to jq.

---

# Explanation of the filter.

- `.data[]`.  Return the contents of the .data array.
- ` | select( .effectiveSlaDomainName != "Unprotected") `. Only return objects with an effectiveSlaDomainName attribute that has a value of `Unprotected`.
- ` | [.name,     .effectiveSlaDomainName] `. Construct a new array of objects with attributes name, and effectiveSlaDomainName.
- ` | @csv `.  Format the output as a list of comma separated values. 

---

# How I run this command line

```bash
export RUBRIK_USERNAME=jsmith39@mail.wvu.edu
export RUBRIK_PASSWORD=MY_PASSWORD
curl -u "${RUBRIK_USERNAME}:${RUBRIK_PASSWORD}" -X GET https://adp-rubrikwvnt.wvu-ad.wvu.edu/api/v1/vmware/vm
```
