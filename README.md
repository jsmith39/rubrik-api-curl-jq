# Using cURL and jq to interact with the Rubrik API.

This presentation was given in the CVS team meeting on 2022-07-19

The presentation can be rendered as html using the [MARP](https://marp.app/) framework.

An HTML rendered version is contained in the public directory.

